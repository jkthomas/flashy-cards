# Flashy Cards

Flashy Cards is an application written in `Python`, using `Kivy`, that allows user to easily created flash cards collections by importing them from text files. Using this method, importing terminologies, translations and definitions lists are really easy without using any 3rd party tools or custom schemas. Specify the file and delimiters, then import the file to start using the flash cards in matter of seconds!

# Setting up development environment

## Requirements:

To be able to launch the project you need:

1. `python3` (preferably 3.9.7)
2. `pip`
3. Packages listed in [requirements file](requirements.txt), that are automatically installed into the virtual environment when the steps below are followed.

Next, you need to install all required tools. You can do that by running:

```
python -m pip install --upgrade pip setuptools virtualenv
```

## Launch setup:

To launch full setup run:

```
. scripts/setup_env.sh
```

This script will create Kivy's virtual environment, change shell to newly created environment and install all required packages from `requirements.txt`.

## Launch app

```
python3 main.py
```

That's it! Enjoy the app!

# Adjust environment without full setup

To activate existing Kivy environment use:

```
. scripts/activate_env.sh
```

To install or update packages use:

```
scripts/install_packages.sh
```

# Current state

The application is in a working state, there are functionalities for:

- importing flash cards from the `.txt` file and naming new collections
- accessing collections by navigating between flash cards with flipping option
- marking the flash cards as correct (done) or required to re-view (redo)

Creating application package for Android systems was in progress before putting the project on hold. It was done using [Buildozer](https://pypi.org/project/buildozer/).

Example views:

- collection view

  ![alt text](static/collection_view_example.png)

- import view

  ![alt text](static/import_view_example.png)
