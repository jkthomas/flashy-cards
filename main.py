from UI.AppScreenManager import AppScreenManager
from kivymd.app import MDApp


class FlashyCards(MDApp):

  def exit_app(self):
    MDApp.get_running_app().stop()

  def build(self):
    self.theme_cls.theme_style = "Dark"
    self.theme_cls.primary_palette = "BlueGray"
    self.theme_cls.accent_palette = "Teal"
    return AppScreenManager(self.exit_app)

  def on_start(self):
    self.fps_monitor_start()


if __name__ == '__main__':
  # TODO: Comment/uncomment as required
  # CollectionsStore().seed_store()
  FlashyCards().run()

# https://buildozer.readthedocs.io/en/latest/installation.html#targeting-android
# https://www.reddit.com/r/kivy/comments/hm1vgm/comment/fx2u1qp/
