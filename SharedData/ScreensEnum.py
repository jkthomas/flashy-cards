from enum import Enum

class ScreensEnum(str, Enum):
  Menu = 'Menu'
  Learn = 'Learn'
  Setup = 'Setup'
