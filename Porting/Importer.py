import uuid
from Store.CollectionsStore import CollectionsStore
from Store.Entities.FlashcardEntity import FlashcardEntity


class Importer():

  def import_collection_file(self, collection_name, file_path, flashcard_delimiter, term_delimiter):
    try:
      with open(file_path, 'r') as f:
        lines = f.readlines()
      # Get rid of newlines and all other whitespaces
      clean_file_lines = [" ".join(line.split()) for line in lines]
      # Create a one-line string from content
      file_content = ''.join(clean_file_lines)

      json_flashcards = []
      for flashcard_raw_data in file_content.split(flashcard_delimiter):
        single_flashcard_content = flashcard_raw_data.split(term_delimiter)
        if(len(single_flashcard_content) == 2):
          json_flashcards.append(FlashcardEntity(
              id=str(uuid.uuid4()),
              term=single_flashcard_content[0].strip(),
              description=single_flashcard_content[1].strip()
          ).toJson())

      if collection_name in CollectionsStore().find_all_names():
        raise Exception('Collection name is already used by store entry')

      CollectionsStore().insert(collection_name=collection_name, cards=json_flashcards)

    except FileNotFoundError:
      print(f"File {file_path} not found. Please provide correct filename.")
    except Exception as ex:
      template = "An exception of type {0} occurred. Arguments:\n{1!r}"
      message = template.format(type(ex).__name__, ex.args)
      print(message)
