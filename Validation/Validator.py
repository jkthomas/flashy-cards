from typing import Callable, Union

from Validation.ValidationResult import ValidationResult

ValidationTypes = Union[str, int, bool]

class Validator(object):
  _input = None
  _input_key = None
  _type = None
  _is_required = False
  _check_is_unique = None
  _no_whitespace = False
  _is_alphanumeric = False
  _min_length = None
  _max_length = None

  def __init__(self, input, input_key: str):
    self._input = input
    self._input_key = input_key

  def set_value(self, value):
    self._input = value

  def type(self, type: ValidationTypes):
    self._type = type
    return self

  def required(self):
    self._is_required = True
    return self

  def unique(self, check_is_unique: Callable[[ValidationTypes], bool]):
    self._check_is_unique = check_is_unique
    return self

  def no_whitespace(self):
    self._no_whitespace = True
    return self

  def alphanumeric(self):
    self._is_alphanumeric = True
    return self

  def min_length(self, min_length: int):
    self._min_length = min_length
    return self

  def max_length(self, max_length: int):
    self._max_length = max_length
    return self

  def validate(self) -> ValidationResult:
    if not self._input:
      raise Exception("Input must be defined for validator")
    if not self._input.name:
      raise Exception("Input name must be defined for validator")

    if self._input_key is not None:
      input_value = getattr(self._input, self._input_key)
    else:
      input_value = self._input

    if self._type is not None and type(input_value) != self._type:
      return ValidationResult(False, f"Incorrect type of provided {self._input.name} value")
    if self._is_required and (input_value is None or len(input_value) == 0):
      return ValidationResult(False, f"Value of {self._input.name} is required")
    if self._check_is_unique is not None and not self._check_is_unique(input_value):
      return ValidationResult(False, f"Value of {self._input.name} must be unique")
    if type(input_value) == str:
      if self._no_whitespace and (' ' in input_value or '\t' in input_value or '\n' in input_value):
        return ValidationResult(False, f"{self._input.name} should not have any whitespaces")
      if self._is_alphanumeric and not input_value.isalnum():
        return ValidationResult(False, f"{self._input.name} should be an alphanumeric value")
      if self._min_length is not None and self._min_length > len(input_value):
        return ValidationResult(False, f"Value of {self._input.name} is too short, its minimal length is {self._min_length}")
      if self._max_length is not None and self._max_length < len(input_value):
        return ValidationResult(False, f"Value of {self._input.name} is too long, its maximal length is {self._max_length}")

    return ValidationResult(True)
