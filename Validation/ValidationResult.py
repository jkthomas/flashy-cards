from typing import Union

class ValidationResult:
  def __init__(self, is_valid: bool, validation_message: Union[None, str] = None) -> None:
    self.is_valid = is_valid
    if not is_valid:
      self.validation_message = validation_message
