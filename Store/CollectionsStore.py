import uuid
from tinydb import TinyDB, Query

from Store.Entities.FlashcardEntity import FlashcardEntity

class CollectionsStore():
  def __init__(self):
    self._store = TinyDB('collections.json')

  def find_one_collection(self, key):
    try:
      return self._store.search(Query()['collection_name'] == key)[0]
    except IndexError:
      return None

  def find_all_names(self):
    collections_names = [collections.get(
        'collection_name') for collections in self._store.search(Query()['collection_name'].exists())]
    return collections_names if collections_names is not None else []

  def insert(self, collection_name, cards):
    self._store.insert({'collection_name': collection_name, 'cards': cards})

  def seed_store(self):
    self._store.truncate()
    self._store.insert({'collection_name': 'words', 'cards': [
        FlashcardEntity(
            id=str(uuid.uuid4()), term='hiatus', description='a short pause in which nothing happens or is said, or a space where something is missing').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='taradiddle',
                        description='pretentious nonsense').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='menagerie',
                        description='a place where animals are kept and trained especially for exhibition').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='scrumptious',
                        description='delightful, excellent').toJson(),
        FlashcardEntity(
            id=str(uuid.uuid4()), term='girth', description=' a band or strap that encircles the body of an animal to fasten something (such as a saddle) on its back').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='preposterous',
                        description='very silly or stupid').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='mundane',
                        description='very ordinary and therefore not interesting').toJson(),
    ]})

    self._store.insert({'collection_name': 'letters', 'cards': [
        FlashcardEntity(
            id=str(uuid.uuid4()), term='a', description='a short pause in which nothing happens or is said, or a space where something is missing').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='b',
                        description='pretentious nonsense').toJson(),
        FlashcardEntity(
            id=str(uuid.uuid4()), term='c', description='a place where animals are kept and trained especially for exhibition').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='d',
                        description='delightful, excellent').toJson(),
        FlashcardEntity(
            id=str(uuid.uuid4()), term='e', description=' a band or strap that encircles the body of an animal to fasten something (such as a saddle) on its back').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='f',
                        description='very silly or stupid').toJson(),

        FlashcardEntity(
            id=str(uuid.uuid4()), term='g', description='very ordinary and therefore not interesting').toJson(),
    ]})

    self._store.insert({'collection_name': 'test1', 'cards': [
        FlashcardEntity(
            id=str(uuid.uuid4()), term='a', description='a short pause in which nothing happens or is said, or a space where something is missing').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='b',
                        description='pretentious nonsense').toJson(),
        FlashcardEntity(
            id=str(uuid.uuid4()), term='c', description='a place where animals are kept and trained especially for exhibition').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='d',
                        description='delightful, excellent').toJson(),
        FlashcardEntity(
            id=str(uuid.uuid4()), term='e', description=' a band or strap that encircles the body of an animal to fasten something (such as a saddle) on its back').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='f',
                        description='very silly or stupid').toJson(),

        FlashcardEntity(
            id=str(uuid.uuid4()), term='g', description='very ordinary and therefore not interesting').toJson(),
    ]})

    self._store.insert({'collection_name': 'test2', 'cards': [
        FlashcardEntity(
            id=str(uuid.uuid4()), term='a', description='a short pause in which nothing happens or is said, or a space where something is missing').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='b',
                        description='pretentious nonsense').toJson(),
        FlashcardEntity(
            id=str(uuid.uuid4()), term='c', description='a place where animals are kept and trained especially for exhibition').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='d',
                        description='delightful, excellent').toJson(),
        FlashcardEntity(
            id=str(uuid.uuid4()), term='e', description=' a band or strap that encircles the body of an animal to fasten something (such as a saddle) on its back').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='f',
                        description='very silly or stupid').toJson(),

        FlashcardEntity(
            id=str(uuid.uuid4()), term='g', description='very ordinary and therefore not interesting').toJson(),
    ]})

    self._store.insert({'collection_name': 'test3', 'cards': [
        FlashcardEntity(
            id=str(uuid.uuid4()), term='a', description='a short pause in which nothing happens or is said, or a space where something is missing').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='b',
                        description='pretentious nonsense').toJson(),
        FlashcardEntity(
            id=str(uuid.uuid4()), term='c', description='a place where animals are kept and trained especially for exhibition').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='d',
                        description='delightful, excellent').toJson(),
        FlashcardEntity(
            id=str(uuid.uuid4()), term='e', description=' a band or strap that encircles the body of an animal to fasten something (such as a saddle) on its back').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='f',
                        description='very silly or stupid').toJson(),

        FlashcardEntity(
            id=str(uuid.uuid4()), term='g', description='very ordinary and therefore not interesting').toJson(),
    ]})

    self._store.insert({'collection_name': 'test4', 'cards': [
        FlashcardEntity(
            id=str(uuid.uuid4()), term='a', description='a short pause in which nothing happens or is said, or a space where something is missing').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='b',
                        description='pretentious nonsense').toJson(),
        FlashcardEntity(
            id=str(uuid.uuid4()), term='c', description='a place where animals are kept and trained especially for exhibition').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='d',
                        description='delightful, excellent').toJson(),
        FlashcardEntity(
            id=str(uuid.uuid4()), term='e', description=' a band or strap that encircles the body of an animal to fasten something (such as a saddle) on its back').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='f',
                        description='very silly or stupid').toJson(),

        FlashcardEntity(
            id=str(uuid.uuid4()), term='g', description='very ordinary and therefore not interesting').toJson(),
    ]})

    self._store.insert({'collection_name': 'long_collection_name', 'cards': [
        FlashcardEntity(
            id=str(uuid.uuid4()), term='hiatus', description='a short pause in which nothing happens or is said, or a space where something is missing').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='taradiddle',
                        description='pretentious nonsense').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='menagerie',
                        description='a place where animals are kept and trained especially for exhibition').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='scrumptious',
                        description='delightful, excellent').toJson(),
        FlashcardEntity(
            id=str(uuid.uuid4()), term='girth', description=' a band or strap that encircles the body of an animal to fasten something (such as a saddle) on its back').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='preposterous',
                        description='very silly or stupid').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='mundane',
                        description='very ordinary and therefore not interesting').toJson(),
        FlashcardEntity(
            id=str(uuid.uuid4()), term='hiatus', description='a short pause in which nothing happens or is said, or a space where something is missing').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='taradiddle',
                        description='pretentious nonsense').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='menagerie',
                        description='a place where animals are kept and trained especially for exhibition').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='scrumptious',
                        description='delightful, excellent').toJson(),
        FlashcardEntity(
            id=str(uuid.uuid4()), term='girth', description=' a band or strap that encircles the body of an animal to fasten something (such as a saddle) on its back').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='preposterous',
                        description='very silly or stupid').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='mundane',
                        description='very ordinary and therefore not interesting').toJson(),
        FlashcardEntity(
            id=str(uuid.uuid4()), term='hiatus', description='a short pause in which nothing happens or is said, or a space where something is missing').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='taradiddle',
                        description='pretentious nonsense').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='menagerie',
                        description='a place where animals are kept and trained especially for exhibition').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='scrumptious',
                        description='delightful, excellent').toJson(),
        FlashcardEntity(
            id=str(uuid.uuid4()), term='girth', description=' a band or strap that encircles the body of an animal to fasten something (such as a saddle) on its back').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='preposterous',
                        description='very silly or stupid').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='mundane',
                        description='very ordinary and therefore not interesting').toJson(),
        FlashcardEntity(
            id=str(uuid.uuid4()), term='hiatus', description='a short pause in which nothing happens or is said, or a space where something is missing').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='taradiddle',
                        description='pretentious nonsense').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='menagerie',
                        description='a place where animals are kept and trained especially for exhibition').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='scrumptious',
                        description='delightful, excellent').toJson(),
        FlashcardEntity(
            id=str(uuid.uuid4()), term='girth', description=' a band or strap that encircles the body of an animal to fasten something (such as a saddle) on its back').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='preposterous',
                        description='very silly or stupid').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='mundane',
                        description='very ordinary and therefore not interesting').toJson(),
        FlashcardEntity(
            id=str(uuid.uuid4()), term='girth', description=' a band or strap that encircles the body of an animal to fasten something (such as a saddle) on its back').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='preposterous',
                        description='very silly or stupid').toJson(),
        FlashcardEntity(id=str(uuid.uuid4()), term='mundane',
                        description='very ordinary and therefore not interesting').toJson(),
    ]})
