import json

class FlashcardEntity():
  def __init__(self, id, term, description):
    self.id = id
    self.term = term
    self.description = description

  def toJson(self):
    return json.loads(json.dumps(self, default=lambda o: o.__dict__))
