from Store.CollectionsStore import CollectionsStore
from UI.Learn.Collection.CollectionDetailsScreen import CollectionDetailsScreen
from UI.Learn.Collection.CollectionsListScreen import CollectionsListScreen
from kivy.uix.screenmanager import ScreenManager

from UI.Learn.Flashcard.FlashcardsListScreen import FlashcardsListScreen

class LearnScreenManager(ScreenManager):

  def __init__(self, **kwargs):
    super(LearnScreenManager, self).__init__(**kwargs)

    self._collections_store = CollectionsStore()

    self._collections_list_screen = CollectionsListScreen(
        name='CollectionsList', handle_collection_selection=self._handle_collection_selection)
    self._collection_details_screen = CollectionDetailsScreen(
        name='CollectionDetails', handle_go_btn_clicked=self._handle_go_btn_clicked)
    self._flashcards_list_screen = FlashcardsListScreen(
        name='FlashcardsList', set_completed_flashcards_counter=self.set_completed_flashcards_counter)

    self.add_widget(self._collections_list_screen)
    self.add_widget(self._collection_details_screen)
    self.add_widget(self._flashcards_list_screen)
    self.current = 'CollectionsList'

  def set_completed_flashcards_counter(self, remaining_cards_count):
    self._collection_details_screen.set_completed_flashcards_counter(remaining_cards_count)

  def _handle_collection_selection(self, selected_collection_name):
    # TODO: Add handling where collection_data is None - nothing was found
    collection_data = self._collections_store.find_one_collection(key=selected_collection_name)
    self._collection_details_screen.set_collection_data(collection_data)
    self._flashcards_list_screen.set_collection_flashcards(collection_data['cards'])
    self.transition.direction = 'left'
    self.current = 'CollectionDetails'

  def _handle_go_btn_clicked(self):
    self.transition.direction = 'left'
    self.current = 'FlashcardsList'

 # TODO: Create proper back button handling - in a special task
  def handle_back_button_clicked(self):
    is_handled_by_internal_manager = False
    if self.current == 'CollectionDetails':
      self.transition.direction = 'right'
      self.current = 'CollectionsList'
      is_handled_by_internal_manager = True
    if self.current == 'FlashcardsList':
      self.transition.direction = 'right'
      self.current = 'CollectionDetails'
      is_handled_by_internal_manager = True
    return is_handled_by_internal_manager
