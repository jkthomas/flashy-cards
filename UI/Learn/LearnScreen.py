from kivy.uix.screenmanager import Screen
from kivymd.uix.button import MDRoundFlatButton
from kivymd.uix.boxlayout import MDBoxLayout
from SharedData.ScreensEnum import ScreensEnum
from kivy.uix.screenmanager import Screen
from kivy.lang import Builder

from UI.Learn.LearnScreenManager import LearnScreenManager

class LearnScreen(Screen):

  def __init__(self, change_screen, **kwargs):
    super(LearnScreen, self).__init__(**kwargs)
    self.add_widget(_LearnLayout(change_screen))


Builder.load_string('''
<_LearnLayout>:
  spacing: 20
  padding: 20
  orientation: 'vertical'
''')

class _LearnLayout(MDBoxLayout):

  def __init__(self, change_screen, **kwargs):
    super(_LearnLayout, self).__init__(**kwargs)
    self._learn_screen_manager = LearnScreenManager()

    back_btn = MDRoundFlatButton(text='Back')

    back_btn.bind(on_release=lambda x: change_screen(
        self._learn_screen_manager.handle_back_button_clicked(), ScreensEnum.Menu))

    self.add_widget(self._learn_screen_manager)
    self.add_widget(back_btn)
