from kivy.uix.screenmanager import Screen
from kivymd.uix.button import MDFillRoundFlatButton
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.label import MDLabel
from kivy.lang import Builder
from UI.Learn.Flashcard.FlashcardsScreenManager import FlashcardsScreenManager

from UI.Shared.CounterLabel import CounterLabel
from UI.Shared.OptionsButtons import OptionsButtons
from UI.Shared.Types.ButtonData import ButtonData


Builder.load_string('''
<_FlashcardsListLayout>:
  spacing: 2
  orientation: 'vertical'
''')
class _FlashcardsListLayout(MDBoxLayout):

  def __init__(self, set_completed_flashcards_counter, flashcards_screen_manager, flip_current_card, set_collection_flashcards, switch_correct_card, switch_incorrect_card, **kwargs):
    super(_FlashcardsListLayout, self).__init__(**kwargs)

    self._counter_label = CounterLabel(size_hint=(1, .1))

    self._set_completed_flashcards_counter = set_completed_flashcards_counter
    self._set_collection_flashcards = set_collection_flashcards
    self._switch_correct_card = switch_correct_card
    self._switch_incorrect_card = switch_incorrect_card
    self._is_finished = False
    self._setup_flashcards(flashcards_screen_manager, flip_current_card)

  def _setup_flashcards(self, flashcards_screen_manager, flip_current_card):
    flip_btn = MDFillRoundFlatButton(text='Flip card', pos_hint={'x': 0.4})
    flip_btn.bind(on_release=lambda x: flip_current_card())

    redo_button_data = ButtonData(text='Redo', on_click=self._switch_incorrect)
    correct_button_data = ButtonData(text='Correct', on_click=self._switch_correct)
    progression_buttons_layout = OptionsButtons(buttons=[redo_button_data, correct_button_data])

    self.add_widget(self._counter_label)
    self.add_widget(flashcards_screen_manager)
    self.add_widget(flip_btn)
    self.add_widget(progression_buttons_layout)

  def set_collection_flashcards(self, collection_flashcards, flashcards_screen_manager, flip_current_card):
    if self._is_finished == True:
      self._is_finished = False
      self.clear_widgets()
      self._setup_flashcards(flashcards_screen_manager, flip_current_card)
    self._counter_label.reset_counter(len(collection_flashcards))
    self._set_collection_flashcards(collection_flashcards)

  def _switch_correct(self, _):
    if self._is_finished is True:
      return

    is_last_card, remaining_cards_count = self._switch_correct_card()
    self._counter_label.update_counter(remaining_cards_count)
    self._set_completed_flashcards_counter(remaining_cards_count)
    if is_last_card:
      self._is_finished = True
      self.clear_widgets()
      congrats_screen = Screen(name='congrats')
      congrats_screen.add_widget(
          MDLabel(text='All flashcards from this collection are completed! Congratulations!', halign='center'))
      self.add_widget(congrats_screen)

  def _switch_incorrect(self, _):
    if self._is_finished is True:
      return
    self._switch_incorrect_card()

# TODO: Handle elegant clean - currently many references stays in memory
class FlashcardsListScreen(Screen):
  def __init__(self, set_completed_flashcards_counter, **kwargs):
    super(FlashcardsListScreen, self).__init__(**kwargs)
    self._flashcards_screen_manager = FlashcardsScreenManager()
    self._flashcards_list_layout = _FlashcardsListLayout(
        set_completed_flashcards_counter,
        self._flashcards_screen_manager,
        self._flip_current_card,
        self._set_collection_flashcards,
        self._switch_correct_card,
        self._switch_incorrect_card
    )
    self.add_widget(self._flashcards_list_layout)

  def set_collection_flashcards(self, collection_flashcards):
    self._flashcards_list_layout.set_collection_flashcards(
        collection_flashcards,
        self._flashcards_screen_manager,
        self._flip_current_card
    )

  def _flip_current_card(self):
    self._flashcards_screen_manager.flip_current_card()

  def _set_collection_flashcards(self, collection_flashcards):
    self._flashcards_screen_manager.set_collection_flashcards(collection_flashcards)

  def _switch_correct_card(self):
    return self._flashcards_screen_manager.switch_correct()

  def _switch_incorrect_card(self):
    self._flashcards_screen_manager.switch_incorrect()
