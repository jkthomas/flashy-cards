from kivy.uix.screenmanager import ScreenManager
from UI.Learn.Flashcard.FlashcardScreen import FlashcardScreen

class FlashcardsScreenManager(ScreenManager):

  def __init__(self, **kwargs):
    super(FlashcardsScreenManager, self).__init__(**kwargs)

  def flip_current_card(self):
    self.current_screen.flip_card()

  def set_collection_flashcards(self, collection_flashcards):
    self.clear_widgets()
    for flashcard_data in collection_flashcards:
      self.add_widget(FlashcardScreen(flashcard_data, name=flashcard_data['id']))
    if self.screen_names:
      self.current = self.screen_names[0]

  def switch_correct(self):
    next_screen_name = self.next()
    is_last_card = (next_screen_name == self.current)
    if not is_last_card:
      self.switch_to(self.get_screen(next_screen_name))

    remaining_cards_count = len(self.screen_names) - 1
    return is_last_card, remaining_cards_count

  def switch_incorrect(self):
    self.current_screen.flip_card_to_term()
    self.current = self.next()
