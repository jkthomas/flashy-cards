from kivymd.uix.label import MDLabel
from kivy.uix.screenmanager import Screen
from kivymd.uix.boxlayout import MDBoxLayout
from kivy.lang import Builder

class FlashcardScreen(Screen):

  def __init__(self, flashcard_data, **kwargs):
    super(FlashcardScreen, self).__init__(**kwargs)
    self._flashcard_layout = _FlashcardLayout(flashcard_data)
    self.add_widget(self._flashcard_layout)

  def flip_card(self):
    self._flashcard_layout.flip_card()

  def flip_card_to_term(self):
    self._flashcard_layout.flip_card_to_term()


Builder.load_string('''
<_FlashcardLayout>:
  spacing: 2
  orientation: 'vertical'
''')

class _FlashcardLayout(MDBoxLayout):

  def __init__(self, flashcard_data, **kwargs):
    super(_FlashcardLayout, self).__init__(**kwargs)

    self._flashcard_data = flashcard_data
    self._title_label = MDLabel(text='Term:', halign='center', size_hint=(1, .1))
    self._text_label = MDLabel(
        text=self._flashcard_data['term'], halign='center', size_hint=(1, .6))

    self.add_widget(self._title_label)
    self.add_widget(self._text_label)

  def flip_card(self):
    if self._text_label.text == self._flashcard_data['term']:
      self._title_label.text = 'Description:'
      self._text_label.text = self._flashcard_data['description']
    else:
      self._title_label.text = 'Term:'
      self._text_label.text = self._flashcard_data['term']

  def flip_card_to_term(self):
    if self._title_label.text == 'Description:':
      self.flip_card()
