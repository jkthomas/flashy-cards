from kivy.uix.screenmanager import Screen
from kivymd.uix.label import MDLabel
from kivymd.uix.button import MDFillRoundFlatButton
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.gridlayout import MDGridLayout
from kivy.uix.scrollview import ScrollView
from kivy.lang import Builder

from Store.CollectionsStore import CollectionsStore

class CollectionsListScreen(Screen):

  # TODO: Refresh ListScreen on Import/Update
  def __init__(self, handle_collection_selection, **kwargs):
    super(CollectionsListScreen, self).__init__(**kwargs)
    collections_store = CollectionsStore()
    collections_names = collections_store.find_all_names()
    self.add_widget(_CollectionsListScreenLayout(handle_collection_selection, collections_names))


Builder.load_string('''
<_CollectionsListScreenLayout>:
  orientation: 'vertical'
''')

class _CollectionsListScreenLayout(MDBoxLayout):

  def __init__(self, handle_collection_selection, collections_names, **kwargs):
    super(_CollectionsListScreenLayout, self).__init__(**kwargs)

    list_view = ScrollView(size_hint=(1, 0.7))
    list_view.add_widget(_CollectionsListLayout(handle_collection_selection, collections_names))

    self.add_widget(MDLabel(text='Collections list', halign='center', size_hint=(1, .2)))
    self.add_widget(list_view)


# TODO: Change to MD List
Builder.load_string('''
<_CollectionsListLayout>:
  cols: 1
  spacing: 10
''')

class _CollectionsListLayout(MDGridLayout):

  def __init__(self, handle_collection_selection, collections_names, **kwargs):
    super(_CollectionsListLayout, self).__init__(**kwargs)

    self.size_hint_y = None
    self.bind(minimum_height=self.setter('height'))

    self.handle_collection_selection = handle_collection_selection
    for name in collections_names:
      self.add_button_to_layout(name, handle_collection_selection)

  def add_button_to_layout(self, name, handle_collection_selection):
    collection_button = MDFillRoundFlatButton(text=name)
    collection_button.background_color = (1.0, 0.0, 0.0, 1.0)
    collection_button.bind(on_release=lambda x: handle_collection_selection(name))
    self.add_widget(collection_button)
