from kivy.uix.gridlayout import GridLayout
from kivymd.uix.label import MDLabel
from kivy.uix.screenmanager import Screen
from kivymd.uix.button import MDFillRoundFlatButton
from kivymd.uix.boxlayout import MDBoxLayout
from kivy.uix.scrollview import ScrollView
from UI.Shared.ColumnHeader import ColumnHeader
from UI.Shared.CounterLabel import CounterLabel
from kivy.lang import Builder

class _FlashcardsListView(ScrollView):
  def __init__(self, **kwargs):
    super(_FlashcardsListView, self).__init__(**kwargs)
    self.list_view_layout = GridLayout(cols=2, size_hint_y=None,
                                       row_force_default=True, row_default_height=40)
    self.list_view_layout.bind(minimum_height=self.list_view_layout.setter('height'))
    self.add_widget(self.list_view_layout)

  def set_list_data(self, flashcards):
    self.list_view_layout.clear_widgets()
    for flashcard in flashcards:
      self.list_view_layout.add_widget(
          MDLabel(text=flashcard['term'], halign='center', size_hint_x=0.25))
      self.list_view_layout.add_widget(
          MDLabel(text=flashcard['description'], halign='center', size_hint_x=0.75))


Builder.load_string('''
<_CollectionDetailsLayout>:
  spacing: 2
  orientation: 'vertical'
''')

class _CollectionDetailsLayout(MDBoxLayout):

  def __init__(self, handle_go_btn_clicked, **kwargs):
    super(_CollectionDetailsLayout, self).__init__(**kwargs)
    self._flashcards_list_view = _FlashcardsListView()
    self._title_label = MDLabel(halign='center', size_hint=(1, .2))
    self._counter_label = CounterLabel(size_hint=(1, .2))

    text_label = MDLabel(text='List:', halign='center', size_hint=(1, .1))

    self._columns_headers = MDBoxLayout(orientation='horizontal', size_hint_y=None, height=50)
    self._columns_headers.add_widget(ColumnHeader(text='Term', size_hint_x=0.25))
    self._columns_headers.add_widget(ColumnHeader(text='Description', size_hint_x=0.75))

    self._go_btn = MDFillRoundFlatButton(text='Go', size_hint=(1, .2))
    self._go_btn.bind(on_release=lambda btn: handle_go_btn_clicked())

    self.add_widget(self._counter_label)
    self.add_widget(self._title_label)
    self.add_widget(text_label)
    self.add_widget(self._columns_headers)
    self.add_widget(self._flashcards_list_view)
    self.add_widget(self._go_btn)

  def set_completed_flashcards_counter(self, remaining_cards_count):
    self._counter_label.update_counter(remaining_cards_count=remaining_cards_count)

  def set_collection_data(self, collection_data):
    self._title_label.text = 'Collection name: ' + collection_data['collection_name']
    self._counter_label.reset_counter(total=len(collection_data['cards']))
    self._flashcards_list_view.set_list_data(collection_data['cards'])
    if collection_data['cards']:
      self._go_btn.disabled = False
    else:
      self._go_btn.disabled = True

class CollectionDetailsScreen(Screen):

  def __init__(self, handle_go_btn_clicked, **kwargs):
    super(CollectionDetailsScreen, self).__init__(**kwargs)
    self._collection_details_layout = _CollectionDetailsLayout(handle_go_btn_clicked)
    self.add_widget(self._collection_details_layout)

  def set_completed_flashcards_counter(self, remaining_cards_count):
    self._collection_details_layout.set_completed_flashcards_counter(remaining_cards_count)

  def set_collection_data(self, collection_data):
    self._collection_details_layout.set_collection_data(collection_data)
