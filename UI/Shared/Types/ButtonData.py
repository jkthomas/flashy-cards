from typing import Callable

class ButtonData:
  def __init__(self, text: str, on_click: Callable) -> None:
    self.text = text
    self.on_click = on_click
