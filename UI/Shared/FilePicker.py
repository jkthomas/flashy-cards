from kivymd.uix.label import MDLabel
from kivymd.uix.button import MDFillRoundFlatButton
from kivymd.uix.filemanager import MDFileManager
from kivymd.uix.boxlayout import MDBoxLayout
from kivy.lang import Builder

Builder.load_string('''
<FilePicker>:
  orientation: 'horizontal'
  size_hint_y: .3
''')

class FilePicker(MDBoxLayout):

  def __init__(self, file_selection_callback, file_picker_label_text='File picked:', **kwargs):
    super(FilePicker, self).__init__(**kwargs)
    self._file_selection_callback = file_selection_callback

    _import_file_button = MDFillRoundFlatButton(
        text='Choose a file',
        size_hint_x=.2,
        pos_hint={"center_y": .5}
    )
    _import_file_button.bind(on_release=self._open_file_manager)
    _file_to_import_label = MDLabel(
        theme_text_color='Secondary',
        halign='center',
        text=file_picker_label_text,
        size_hint_x=.2,
    )
    self._file_path_label = MDLabel(size_hint_x=.6)
    self.add_widget(_import_file_button)
    self.add_widget(_file_to_import_label)
    self.add_widget(self._file_path_label)

    self.file_manager = MDFileManager(
        exit_manager=self._handle_file_manager_exit,
        select_path=self._handle_file_selection,
        selector='file'
    )

  def _handle_file_manager_exit(self, _):
    self.file_manager.close()

  def _open_file_manager(self, _):
    # TODO: Set some default directory for mobile
    self.file_manager.show('/home/tomek/Projects/flashy-cards')

  def _handle_file_selection(self, path):
    self._file_path_label.text = path
    self._file_selection_callback(path)
    self.file_manager.close()
