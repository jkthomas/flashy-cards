from kivymd.uix.label import MDLabel
from kivy.lang import Builder

Builder.load_string('''
<ColumnHeader>:
  halign: 'center'
  md_bg_color: app.theme_cls.primary_dark
''')


class ColumnHeader(MDLabel):
  def __init__(self, **kwargs):
    super(ColumnHeader, self).__init__(**kwargs)
