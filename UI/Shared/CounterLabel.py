from kivymd.uix.label import MDLabel
from kivy.lang import Builder

Builder.load_string('''
<CounterLabel>:
  halign: 'center'
  md_bg_color: app.theme_cls.primary_dark
''')

class CounterLabel(MDLabel):

  def __init__(self, total=0, **kwargs):
    super(CounterLabel, self).__init__(**kwargs)
    self._total = total
    self.reset_counter(0)

  def reset_counter(self, total):
    self._total = total
    self.text = f"Completed: 0/{self._total}"

  def update_counter(self, remaining_cards_count):
    self.text = f"Completed: {self._total - remaining_cards_count}/{self._total}"
