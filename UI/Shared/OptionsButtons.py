from kivy.uix.screenmanager import Screen
from kivymd.uix.button import MDFillRoundFlatButton
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.label import MDLabel
from kivy.lang import Builder
from typing import List

from UI.Shared.CounterLabel import CounterLabel
from UI.Shared.Types.ButtonData import ButtonData


Builder.load_string('''
<OptionsButtons>:
  orientation: 'horizontal'
  spacing: 10
  size_hint: (1, 0.1)
''')
class OptionsButtons(MDBoxLayout):

  def __init__(self, buttons: List[ButtonData], orientation: str = 'horizontal', **kwargs):
    super(OptionsButtons, self).__init__(**kwargs)
    self.orientation = orientation

    for button in buttons:
      self.add_widget(self._get_button(text=button.text, callback=button.on_click))

  def _get_button(self, text, callback):
    button = MDFillRoundFlatButton(text=text)
    button.bind(on_release=callback)
    return button
