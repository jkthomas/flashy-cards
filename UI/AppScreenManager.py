from UI.Menu.MenuScreen import MenuScreen
from UI.Learn.LearnScreen import LearnScreen
from kivy.uix.screenmanager import ScreenManager
from SharedData.ScreensEnum import ScreensEnum
from UI.Setup.SetupScreen import SetupScreen


class AppScreenManager(ScreenManager):

  def __init__(self, exit_app_callback, **kwargs):
    super(AppScreenManager, self).__init__(**kwargs)
    self.menuScreen = MenuScreen(exit_app_callback,
                                 change_screen=self._change_screen, name=ScreensEnum.Menu)
    self.learnScreen = LearnScreen(change_screen=self._change_screen, name=ScreensEnum.Learn)
    self.setupScreen = SetupScreen(change_screen=self._change_screen,
                                   handle_file_imported=self._handle_file_imported, name=ScreensEnum.Setup)
    self.add_widget(self.menuScreen)
    self.add_widget(self.learnScreen)
    self.add_widget(self.setupScreen)

 # TODO: Use previous() and next() for screens handling instead of handling by static name
 # TODO: Create proper back button handling - in a special task
  def _change_screen(self, is_handled_by_internal_manager, destination_screen_name):
    if is_handled_by_internal_manager:
      return
    if destination_screen_name == ScreensEnum.Menu:
      self.transition.direction = 'right'
    else:
      self.transition.direction = 'left'

    if self.has_screen(destination_screen_name):
      self.current = destination_screen_name
    else:
      print('Error: No screen with provided name found')

  def _handle_file_imported(self):
    # TODO: Need a cleanup system to make it work - nested references error
    # Reload learn screen
    self.clear_widgets()
    self.learnScreen = LearnScreen(change_screen=self._change_screen, name=ScreensEnum.Learn)
    self.add_widget(self.menuScreen)
    self.add_widget(self.learnScreen)
    self.add_widget(self.setupScreen)

    # Swap to Menu screen
    self.current = ScreensEnum.Menu
