from kivy.uix.screenmanager import Screen
from kivymd.uix.label import MDLabel
from kivymd.uix.button import MDRoundFlatButton, MDFillRoundFlatButton
from kivymd.uix.boxlayout import MDBoxLayout
from SharedData.ScreensEnum import ScreensEnum
from kivy.lang import Builder

class MenuScreen(Screen):

  def __init__(self, exit_btn_clicked, change_screen, **kwargs):
    super(MenuScreen, self).__init__(**kwargs)
    self.add_widget(_MenuLayout(exit_btn_clicked, change_screen))


Builder.load_string('''
<_MenuLayout>:
  spacing: 30
  padding: 20
  orientation: 'vertical'
''')

class _MenuLayout(MDBoxLayout):

  def __init__(self, exit_btn_clicked, change_screen, **kwargs):
    super(_MenuLayout, self).__init__(**kwargs)

    learn_btn = MDFillRoundFlatButton(text='Learn', size_hint=(1, .3))
    learn_btn.bind(on_release=lambda _btn: change_screen(
        is_handled_by_internal_manager=False, destination_screen_name=ScreensEnum.Learn))
    setup_btn = MDFillRoundFlatButton(text='Setup', size_hint=(1, .3))
    setup_btn.bind(on_release=lambda _btn: change_screen(
        is_handled_by_internal_manager=False, destination_screen_name=ScreensEnum.Setup))
    exit_btn = MDRoundFlatButton(text='Exit', size_hint=(1, .2))
    exit_btn.bind(on_release=lambda _btn: exit_btn_clicked())

    self.add_widget(MDLabel(text='Flashy Cards', halign='center'))
    self.add_widget(learn_btn)
    self.add_widget(setup_btn)
    self.add_widget(exit_btn)
