from kivymd.uix.label import MDLabel
from kivymd.uix.button import MDFillRoundFlatButton
from kivy.uix.screenmanager import Screen
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.textfield import MDTextField
from kivy.lang import Builder
from kivymd.toast import toast

from Porting.Importer import Importer
from Store.CollectionsStore import CollectionsStore
from UI.Shared.FilePicker import FilePicker
from Validation.Validator import Validator

Builder.load_string('''
<_ImportScreenLayout>:
  spacing: 3
  orientation: 'vertical'
''')

class _ImportScreenLayout(MDBoxLayout):

  def __init__(self, handle_file_imported, **kwargs):
    super(_ImportScreenLayout, self).__init__(**kwargs)
    self._handle_file_imported = handle_file_imported
    self._title_label = MDLabel(text='Import a collection', halign='center', size_hint=(1, .2))
    self._input_validators = []
    self._import_file_path = ''

    self.add_widget(self._title_label)

    self._collection_name_input = MDTextField(
        mode='rectangle',
        hint_text='Collection name',
        size_hint_x=.9,
        pos_hint={"center_x": .5}
    )
    self._collection_name_input.name = 'Collection name'

    self._flashcard_delimiter_input = MDTextField(
        mode='rectangle',
        hint_text='Flashcard delimiter',
        size_hint_x=.9,
        pos_hint={"center_x": .5}
    )
    self._flashcard_delimiter_input.name = 'Flashcard delimiter'

    self._term_delimiter_input = MDTextField(
        mode='rectangle',
        hint_text='Term delimiter',
        size_hint_x=.9,
        pos_hint={"center_x": .5}
    )
    self._term_delimiter_input.name = 'Term delimiter'

    # TODO: Change to error handling with error message on the input field
    def check_name_uniqueness(value) -> bool:
      if value not in CollectionsStore().find_all_names():
        return True
      return False

    self._input_validators = [
        Validator(input=self._collection_name_input, input_key='text').type(
            str).required().unique(check_is_unique=check_name_uniqueness).no_whitespace().min_length(3).max_length(20),
        Validator(input=self._flashcard_delimiter_input,
                  input_key='text').type(str).required().max_length(3),
        Validator(input=self._term_delimiter_input, input_key='text').type(
            str).required().max_length(3)
    ]

    _file_picker = FilePicker(self._set_imported_file_path, 'File to import:')

    _import_button = MDFillRoundFlatButton(text='Import selected file', pos_hint={"center_x": .5})
    _import_button.bind(on_release=self._on_import_clicked)

    self.add_widget(self._collection_name_input)
    self.add_widget(self._flashcard_delimiter_input)
    self.add_widget(self._term_delimiter_input)
    self.add_widget(_file_picker)
    self.add_widget(_import_button)

  def _set_imported_file_path(self, path):
    self._import_file_path = path

  def _on_import_clicked(self, _btn):
    if not self._import_file_path:
      toast('No import file selected')
      return
    for validator in self._input_validators:
      validation_result = validator.validate()
      if validation_result.is_valid is not True:
        toast(validation_result.validation_message)
        return
    Importer().import_collection_file(
        self._collection_name_input.text,
        self._import_file_path,
        self._flashcard_delimiter_input.text,
        self._term_delimiter_input.text
    )
    toast(f"Collection {self._collection_name_input.text} has been imported successfully")
    self._handle_file_imported()

class ImportScreen(Screen):

  def __init__(self, handle_file_imported, **kwargs):
    super(ImportScreen, self).__init__(**kwargs)
    self._import_layout = _ImportScreenLayout(handle_file_imported)
    self.add_widget(self._import_layout)
