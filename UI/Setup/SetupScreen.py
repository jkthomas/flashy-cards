from kivy.uix.screenmanager import Screen
from kivymd.uix.button import MDRoundFlatButton
from kivymd.uix.boxlayout import MDBoxLayout
from SharedData.ScreensEnum import ScreensEnum
from kivy.lang import Builder

from UI.Setup.SetupScreenManager import SetupScreenManager

class SetupScreen(Screen):

  def __init__(self, change_screen, handle_file_imported, **kwargs):
    super(SetupScreen, self).__init__(**kwargs)
    self.add_widget(_SetupLayout(change_screen, handle_file_imported))


Builder.load_string('''
<_SetupLayout>:
  spacing: 20
  padding: 20
  orientation: 'vertical'
''')

class _SetupLayout(MDBoxLayout):

  def __init__(self, change_screen, handle_file_imported, **kwargs):
    super(_SetupLayout, self).__init__(**kwargs)
    self._setup_screen_manager = SetupScreenManager(handle_file_imported)

    back_btn = MDRoundFlatButton(text='Back')
    back_btn.bind(on_release=lambda x: change_screen(
        self._setup_screen_manager.handle_back_button_clicked(), ScreensEnum.Menu))

    self.add_widget(self._setup_screen_manager)
    self.add_widget(back_btn)
