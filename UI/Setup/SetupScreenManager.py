from kivy.uix.screenmanager import ScreenManager

from UI.Setup.Import.ImportScreen import ImportScreen

class SetupScreenManager(ScreenManager):

  def __init__(self, handle_file_imported, **kwargs):
    super(SetupScreenManager, self).__init__(**kwargs)

    self._import_screen = ImportScreen(handle_file_imported, name='ImportScreen')

    self.add_widget(self._import_screen)
    self.current = 'ImportScreen'

  def handle_back_button_clicked(self):
    # TODO: Extend on Setup feature implementation
    is_handled_by_internal_manager = False
    return is_handled_by_internal_manager
